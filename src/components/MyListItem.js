import React from "react";
import { Text, StyleSheet, View, TouchableOpacity } from "react-native";
import moment from "moment";
import { Navigation } from "react-native-navigation";

import constants from "../config/constants";

const { height, width, size, appraiseArray } = constants;

const MyListItem = props => {
  const redirectToAppraise = () => {
    Navigation.push(props.componentId, {
      component: {
        name: "clean.Appraise"
      }
    });
  };

  console.log(props);
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={{
        width,
        height: height * 0.11,
        borderBottomWidth: 1,
        borderBottomColor: "#b7b7b7",
        padding: 10
      }}
      onPress={redirectToAppraise}
    >
      <View
        style={{
          width: width * 0.9,
          height: height * 0.03,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between"
        }}
      >
        <Text style={{ fontWeight: "bold", fontSize: size.small }}>
          Cotización [{props.id}]
        </Text>
        <Text style={{ color: "#b7b7b7", fontSize: size.small }}>
          {moment(props.fecha_creacion).format("DD MMMM YYYY")}
        </Text>
      </View>
      <View
        style={{
          height: height * 0.05,
          alignItems: "center",
          flexDirection: "row"
        }}
      >
        <Text>Estado: </Text>
        {props.status === 1 ? (
          <Text style={{ color: "#469CDD", fontWeight: "bold" }}>
            Confirmado
          </Text>
        ) : (
          <Text style={{ color: "#b7b7b7", fontWeight: "bold" }}>
            Pendiente
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default MyListItem;
