import React from "react";
import { Text, StyleSheet, TouchableOpacity, Image, View } from "react-native";
import constants from "../config/constants";

const { width, height, size } = constants;

const Gender = ({ genderText, handleActive, id, activeGender }) => (
  <TouchableOpacity
    onPress={() => handleActive(id)}
    style={[
      styles.itemButton,
      {
        backgroundColor: "transparent",
        borderColor: activeGender ? "#469CDD" : "#b7b7b7"
      }
    ]}
  >
    <Text
      style={{
        fontSize: size.normal,
        color: activeGender ? "#469CDD" : "#b7b7b7"
      }}
    >
      {genderText}
    </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  itemButton: {
    marginHorizontal: 4,
    marginVertical: 5,
    width: width * 0.18,
    height: height * 0.08,
    borderRadius: 5,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5
  },
  buttonText: { color: "#fff", fontSize: size.small, textAlign: "center" }
});

export default Gender;
