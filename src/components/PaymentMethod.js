import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import Method from "./Method";

import constants from "../config/constants";

const { width, height } = constants;

export default class PaymentMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: 1
    };

    this.handleActive = this.handleActive.bind(this);
    this.activeButton = this.activeButton.bind(this);
  }

  handleActive(activeButton) {
    this.setState(
      {
        activeButton
      },
      () => this.props.setPaymentMethod(activeButton)
    );
  }

  activeButton(id) {
    return id === this.state.activeButton;
  }

  render() {
    return (
      <View
        style={{
          width: width * 0.9,
          height: height * 0.18,
          alignSelf: "center",
          justifyContent: "center"
        }}
      >
        <Text>Elegir método de pago</Text>
        <View
          style={{
            flexDirection: "row",
            width: width * 0.9,
            height: height * 0.1,
            alignItems: "flex-end",
            justifyContent: "space-evenly"
          }}
        >
          <Method
            methodName={"Efectivo"}
            id={1}
            handleActive={this.handleActive}
            activeButton={this.activeButton(1)}
          />
          <Method
            methodName={"Tarjeta"}
            id={2}
            handleActive={this.handleActive}
            activeButton={this.activeButton(2)}
          />
        </View>
      </View>
    );
  }
}
