import React from "react";
import { Text, StyleSheet, TouchableOpacity, Image, View } from "react-native";
import constants from "../config/constants";

const { width, height, size } = constants;

const Task = props => (
  <TouchableOpacity
    onPress={() => props.handleActive(props.id)}
    style={[
      styles.itemButton,
      {
        backgroundColor: "transparent",
        borderColor: props.activeButton ? "#469CDD" : "#b7b7b7"
      }
    ]}
  >
    <View>
      <Image
        source={props.imageTaks}
        style={{
          resizeMode: "contain",
          width: width * 0.1,
          tintColor: props.activeButton ? "#469CDD" : "#b7b7b7"
        }}
      />
    </View>
    <Text
      style={[
        styles.buttonText,
        { color: props.activeButton ? "#469CDD" : "#b7b7b7" }
      ]}
    >
      {props.taskName}
    </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  itemButton: {
    marginHorizontal: 4,
    marginVertical: 5,
    width: width * 0.2,
    height: height * 0.16,
    borderRadius: 5,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "flex-end",
    padding: 5
  },
  buttonText: { color: "#fff", fontSize: size.small, textAlign: "center" }
});

export default Task;
