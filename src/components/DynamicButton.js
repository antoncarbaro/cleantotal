import React from "react";
import { View } from "react-native";
import config from "../config/constants";
import ButtonDynamic from "./ButtonDynamic";
// import img from "../config/images";

const { height, width } = config;

export default class DynamicButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButtonId: 0
    };

    this.onHandleActive = this.onHandleActive.bind(this);
  }

  onHandleActive(idButton) {
    this.setState({ activeButtonId: idButton });
    this.props.dynamicFunction(idButton);
  }

  activeButton(id) {
    return id === this.state.activeButtonId;
  }

  render() {
    return (
      <View
        style={{
          width: width * 0.8,
          height: height * 0.2,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <ButtonDynamic
          number={this.props.number === 10 ? 12 : 1}
          activeButton={this.activeButton(this.props.number === 10 ? 12 : 1)}
          onHandleActive={this.onHandleActive}
        />
        <ButtonDynamic
          number={this.props.number === 10 ? 24 : 2}
          activeButton={this.activeButton(this.props.number === 10 ? 24 : 2)}
          onHandleActive={this.onHandleActive}
        />
        <ButtonDynamic
          number={this.props.number === 10 ? 48 : 3}
          activeButton={this.activeButton(this.props.number === 10 ? 48 : 3)}
          onHandleActive={this.onHandleActive}
        />
      </View>
    );
  }
}
