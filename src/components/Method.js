import React from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";

import constants from "../config/constants";

const { width, height, size } = constants;

const Method = ({ methodName, handleActive, id, activeButton }) => (
  <TouchableOpacity
    onPress={() => handleActive(id)}
    style={[
      styles.itemButton,
      {
        backgroundColor: "transparent",
        borderColor: activeButton ? "#469CDD" : "#b7b7b7",
        borderWidth: activeButton ? 1.5 : 1
      }
    ]}
  >
    <Text
      style={[
        styles.buttonText,
        { color: activeButton ? "#469CDD" : "#b7b7b7" },
        { fontWeight: activeButton ? "bold" : "100" }
      ]}
    >
      {methodName}
    </Text>
  </TouchableOpacity>
);

export default Method;

const styles = StyleSheet.create({
  itemButton: {
    marginHorizontal: 4,
    marginVertical: 5,
    width: width * 0.2,
    height: height * 0.07,
    borderRadius: 5,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5
  },
  buttonText: { color: "#fff", fontSize: size.little, textAlign: "center" }
});
