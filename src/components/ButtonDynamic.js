import React from "react";
import { Text, TouchableOpacity } from "react-native";

import config from "../config/constants";

const { height, width, size } = config;

const ButtonDynamic = props => {
  return (
    <TouchableOpacity
      onPress={() => props.onHandleActive(props.number)}
      activeOpacity={0.6}
      style={{
        backgroundColor: props.activeButton ? "#fff" : "transparent",
        borderColor: "#fff",
        borderWidth: 1,
        width: width * 0.2,
        height: height * 0.1,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5
      }}
    >
      <Text
        style={{
          color: props.activeButton ? "#c3c3c3" : "#fff",
          fontWeight: "100",
          fontSize: size.normal
        }}
      >
        {props.number}
      </Text>
    </TouchableOpacity>
  );
};

export default ButtonDynamic;
