import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  KeyboardAvoidingView
} from "react-native";

import constants from "../config/constants";
import DynamicButton from "./DynamicButton";
import FloortType from "./FloorType";

const { height, width, size } = constants;

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quarterMetters: "",
      pickedFloorType: null,
      pickedWindowType: null
    };
    this.inputRefs = {};
    this.onHandleChange = this.onHandleChange.bind(this);
    this.dynamicFunction = this.dynamicFunction.bind(this);
  }

  onHandleChange(quarterMetters) {
    this.setState({ quarterMetters });
    this.props.handleChange(quarterMetters);
  }

  dynamicFunction(value) {
    this.props.dynamicFunction(value);
  }

  render() {
    return (
      <View style={{ height: height * 0.7, alignItems: "center" }}>
        <View
          style={{
            width: width * 0.8,
            height: height * 0.15,
            justifyContent: "flex-end",
            alignItems: "center"
          }}
        >
          <Text
            style={{ color: "#fff", fontSize: size.great, textAlign: "center" }}
          >
            {this.props.title}
          </Text>
        </View>
        {this.props.number !== 2 ? (
          this.props.number === 0 ? (
            <KeyboardAvoidingView
              keyboardVerticalOffset={100}
              behavior="padding"
              enabled
              style={{
                width,
                height: height * 0.2,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  width: width * 0.8,
                  height: height * 0.08
                }}
              >
                <TextInput
                  keyboardType={"numeric"}
                  value={this.state.quarterMetters}
                  placeholder={"Metros cuadrados"}
                  placeholderTextColor={"#fff"}
                  onChangeText={quarterMetters =>
                    this.onHandleChange(quarterMetters)
                  }
                  style={{
                    borderWidth: 1,
                    borderColor: "#fff",
                    height: height * 0.08,
                    color: "#fff"
                  }}
                />
              </View>
            </KeyboardAvoidingView>
          ) : (
            <DynamicButton
              dynamicFunction={this.dynamicFunction}
              number={this.props.number}
            />
          )
        ) : (
          <FloortType dynamicFunction={this.dynamicFunction} />
        )}
        <View
          style={{
            width,
            height: height * 0.2,
            justifyContent:
              this.props.number !== 0 ? "space-between" : "center",
            alignItems: "flex-end",
            flexDirection: "row"
          }}
        >
          {this.props.number !== 0 && (
            <TouchableOpacity
              onPress={() => this.props.scrollTo(this.props.number - 1)}
              style={{
                width: width * 0.5,
                height: height * 0.1,
                borderRadius: 10,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: size.max,
                  textAlign: "center",
                  fontWeight: "300"
                }}
              >
                Anterior
              </Text>
            </TouchableOpacity>
          )}
          {this.props.number !== 10 ? (
            <TouchableOpacity
              onPress={() => this.props.scrollTo(this.props.number + 1)}
              style={{
                width: width * 0.5,
                height: height * 0.1,
                borderRadius: 10,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: size.max,
                  textAlign: "center",
                  fontWeight: "300"
                }}
              >
                Siguiente
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={this.props.finishAppraise}
              style={{
                width: width * 0.5,
                height: height * 0.1,
                borderRadius: 10,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: size.max,
                  textAlign: "center",
                  fontWeight: "300"
                }}
              >
                Finalizar
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputAndroid: {
    color: "#fff"
  },
  inputIOS: {
    fontSize: 16,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderRadius: 4,
    color: "#fff"
  }
});
