import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";
import constants from "../config/constants";
import Task from "./Task";
import img from "../config/images";

const { height, width, size } = constants;

export default class ExtraTasks extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeButton: []
    };
    this.handleActive = this.handleActive.bind(this);
    this.activeButton = this.activeButton.bind(this);
  }

  handleActive(activeButton) {
    if (!this.state.activeButton.includes(activeButton)) {
      let totalButtons = [...this.state.activeButton, activeButton];
      this.setState(
        {
          activeButton: totalButtons
        },
        () => this.props.extraTasks(this.state.activeButton)
      );
    } else {
      let totalButtons = this.remove(this.state.activeButton, activeButton);
      this.setState(
        {
          activeButton: totalButtons
        },
        () => this.props.extraTasks(this.state.activeButton)
      );
    }
  }

  remove(array, element) {
    return array.filter(el => el !== element);
  }

  activeButton(id) {
    return this.state.activeButton.includes(id);
  }

  render() {
    return (
      <View style={styles.extraTasksContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Añade otras tareas extra</Text>
        </View>
        <View style={styles.extraTaksksButtonsContainer}>
          {/* <Task
            imageTaks={img.ironingIcon}
            taskName={"Planchar Ropa"}
            id={1}
            handleActive={this.handleActive}
            activeButton={this.activeButton(1)}
          /> */}
          <Task
            imageTaks={img.fridgeIcon}
            taskName={"Interior Frigorífico"}
            id={1}
            handleActive={this.handleActive}
            activeButton={this.activeButton(1)}
          />
          <Task
            imageTaks={img.ovenIcon}
            taskName={"Interior Horno"}
            id={2}
            handleActive={this.handleActive}
            activeButton={this.activeButton(2)}
          />
          <Task
            imageTaks={img.wardroupeIcon}
            taskName={"Interior Muebles"}
            id={3}
            handleActive={this.handleActive}
            activeButton={this.activeButton(3)}
          />
          <Task
            imageTaks={img.windowIcon}
            taskName={"Interior Ventanas"}
            id={4}
            handleActive={this.handleActive}
            activeButton={this.activeButton(4)}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  extraTasksContainer: {
    width,
    height: height * 0.45,
    alignItems: "center"
  },
  titleContainer: {
    height: height * 0.08,
    justifyContent: "center"
  },
  title: { color: "#000", fontSize: size.regular },
  extraTaksksButtonsContainer: {
    width: width * 0.8,
    height: height * 0.35,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly"
  }
});
