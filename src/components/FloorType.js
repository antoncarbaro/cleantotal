import React from "react";
import { View } from "react-native";
import config from "../config/constants";
import FloorButton from "./FloorButton";

const { width, height } = config;

export default class FloortType extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButtonId: 0
    };

    this.onHandleActive = this.onHandleActive.bind(this);
  }

  onHandleActive(idButton) {
    this.setState({ activeButtonId: idButton });
    this.props.dynamicFunction(idButton);
  }

  activeButton(id) {
    return id === this.state.activeButtonId;
  }

  render() {
    return (
      <View
        style={{
          width: width * 0.8,
          flexWrap: "wrap",
          height: height * 0.2,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "flex-start",
          padding: 10
        }}
      >
        <FloorButton
          number={1}
          activeButton={this.activeButton(1)}
          onHandleActive={this.onHandleActive}
          label={"Parquet"}
        />
        <FloorButton
          number={2}
          activeButton={this.activeButton(2)}
          onHandleActive={this.onHandleActive}
          label={"Porcelanato"}
        />
        <FloorButton
          number={3}
          activeButton={this.activeButton(3)}
          onHandleActive={this.onHandleActive}
          label={"Alfombrado"}
        />
        <FloorButton
          number={4}
          activeButton={this.activeButton(4)}
          onHandleActive={this.onHandleActive}
          label={"Estandar"}
        />
      </View>
    );
  }
}
