import React from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import config from "../config/constants";

const { width, height, size } = config;

const WelcomePage = props => {
  return (
    <View
      style={{
        width,
        height: height * 0.7,
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <View style={{ height: height * 0.12, width }} />
      <View
        style={{
          width: width * 0.8,
          height: height * 0.5,
          alignItems: "center",
          backgroundColor: "#fff",
          justifyContent: "space-evenly",
          borderRadius: 10
        }}
      >
        <Image
          source={props.imageLogo}
          style={{
            resizeMode: "contain",
            width: width * 0.5,
            height: height * 0.5
          }}
        />
        <View
          style={{
            width: width * 0.8,
            height: height * 0.18,
            alignItems: "center",
            justifyContent: "flex-start"
          }}
        >
          <Text
            style={{
              fontSize: size.max,
              color: "#469CDD",
              fontWeight: "400",
              textAlign: "center"
            }}
          >
            {props.welcomeText}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default WelcomePage;
