import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";

import constants from "../config/constants";
import Hours from "./Hours";

const { height, width, size } = constants;

export default class ExtraHours extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: 1
    };

    this.handleActive = this.handleActive.bind(this);
    this.activeButton = this.activeButton.bind(this);
  }

  handleActive(activeButton) {
    this.setState(
      {
        activeButton
      },
      () => this.props.extraHours(this.state.activeButton)
    );
  }

  activeButton(id) {
    return id === this.state.activeButton;
  }

  render() {
    return (
      <View style={styles.extraTasksContainer}>
        <View
          style={{
            width: width * 0.9,
            height: height * 0.08,
            justifyContent: "center",
            alignItems: "flex-end"
          }}
        >
          <Text style={{ fontWeight: "bold", fontSize: size.little }}>
            {" "}
            Tener presente que:{" "}
            <Text style={{ fontWeight: "100" }}>
              Trabajamos hasta las 07:00 PM
            </Text>
          </Text>
        </View>
        <View style={styles.extraHoursContainer}>
          <Hours
            hourText={"6 horas"}
            id={1}
            handleActive={this.handleActive}
            activeButton={this.activeButton(1)}
          />
          <Hours
            hourText={"24 horas"}
            id={2}
            handleActive={this.handleActive}
            activeButton={this.activeButton(2)}
          />
          <Hours
            hourText={"48 horas"}
            id={3}
            handleActive={this.handleActive}
            activeButton={this.activeButton(3)}
          />
          <View
            style={{
              height: height * 0.05,
              width,
              justifyContent: "center",
              paddingLeft: 25
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: size.regular }}>
              Recargo por tiempo:{" "}
              <Text style={{ fontWeight: "100" }}>
                {this.state.activeButton === 1 && `50% de recargo.`}
                {this.state.activeButton === 2 && `25% de recargo.`}
                {this.state.activeButton === 3 && `0% de recargo.`}
              </Text>{" "}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  extraTasksContainer: {
    width,
    height: height * 0.25
  },
  extraHoursContainer: {
    width,
    height: height * 0.18,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "space-evenly",
    paddingTop: 10
  }
});
