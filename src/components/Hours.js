import React from "react";
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import constants from "../config/constants";

const { width, height, size } = constants;
const Hours = props => (
  <TouchableOpacity
    onPress={() => props.handleActive(props.id)}
    style={[
      styles.itemButton,
      {
        backgroundColor: "transparent",
        borderColor: props.activeButton ? "#469CDD" : "#b7b7b7",
        borderWidth: props.activeButton ? 1.5 : 1
      }
    ]}
  >
    <Text
      style={[
        styles.buttonText,
        { color: props.activeButton ? "#469CDD" : "#b7b7b7" },
        { fontWeight: props.activeButton ? "bold" : "100" }
      ]}
    >
      {props.hourText}
    </Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  itemButton: {
    marginHorizontal: 4,
    marginVertical: 5,
    width: width * 0.25,
    height: height * 0.08,
    borderRadius: 5,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 5
  },
  buttonText: { color: "#fff", fontSize: size.regular, textAlign: "center" }
});

export default Hours;
