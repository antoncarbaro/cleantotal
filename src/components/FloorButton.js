import React from "react";
import { Text, TouchableOpacity } from "react-native";

import config from "../config/constants";

const { width, height, size } = config;

const FloorButton = props => (
  <TouchableOpacity
    onPress={() => props.onHandleActive(props.number)}
    style={{
      backgroundColor: props.activeButton ? "#fff" : "transparent",
      borderWidth: 1,
      borderColor: "#fff",
      borderRadius: 5,
      justifyContent: "center",
      alignItems: "center",
      margin: 5,
      width: width * 0.32,
      height: height * 0.1
    }}
  >
    <Text
      style={{
        color: props.activeButton ? "#c3c3c3" : "#fff",
        fontWeight: "100",
        fontSize: size.normal
      }}
    >
      {props.label}
    </Text>
  </TouchableOpacity>
);

export default FloorButton;
