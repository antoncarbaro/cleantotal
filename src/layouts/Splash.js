import React from "react";
import { Image, ImageBackground, AsyncStorage } from "react-native";
import { Navigation } from "react-native-navigation";

import constants from "../config/constants";
import img from "../config/images";

const { height, width } = constants;

export default class Splash extends React.Component {
  componentDidMount() {
    setTimeout(async () => {
      let user = await JSON.parse(await AsyncStorage.getItem("user"));
      if (user) {
        Navigation.push(this.props.componentId, {
          component: {
            name: "clean.Home"
          }
        });
      } else {
        let hasSeenApp = await AsyncStorage.getItem("hasSeenApp");
        await AsyncStorage.setItem("hasSeenApp", "true");
        Navigation.push(this.props.componentId, {
          component: {
            name: hasSeenApp ? "clean.Login" : "clean.Welcome"
          }
        });
      }
    }, 1000);
  }

  render() {
    return (
      <ImageBackground
        source={img.splashBackground}
        style={{
          backgroundColor: "#fff",
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={img.logoCleaner}
          style={{
            width: width * 0.65,
            height: height * 0.5,
            resizeMode: "contain"
          }}
        />
      </ImageBackground>
    );
  }
}
