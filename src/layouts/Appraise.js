import React, { Component } from "react";
import {
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  StyleSheet,
  Text,
  ScrollView,
  ActivityIndicator
} from "react-native";
import { Navigation } from "react-native-navigation";
import RNPickerSelect from "react-native-picker-select";
// import AppraisePage from "../components/AppraisePage";

import constants from "../config/constants";
import img from "../config/images";
import url from "../config/url";

import ExtraTasks from "../components/ExtraTasks";
import ExtraHours from "../components/ExtraHours";
import Resumen from "../layouts/Resumen";
import PaymentMethod from "../components/PaymentMethod";

const { height, width, dotsIndex, size } = constants;

export default class Appraise extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      meters: "1",
      dotsIndex: [],
      index: 0,
      quarterMetters: "",
      onFocusInput: false,
      onFocusFloor: false,
      aditionalInfo: "",
      supplies: true,
      movility: false,
      loader: false,
      storedLocation: false,
      region: {},
      floorType: [
        { label: "Parquet", value: 1 },
        { label: "Porcelanato", value: 2 },
        { label: "Alfombrado", value: 3 },
        { label: "Estandar", value: 4 }
      ],
      roomNumber: [
        { label: "1 habitación", value: 1 },
        { label: "2 habitaciones", value: 2 },
        { label: "3 habitaciones", value: 3 },
        { label: "4 habitaciones", value: 4 },
        { label: "5 habitaciones", value: 5 }
      ],
      bathroomNumber: [
        { label: "1 baño", value: 1 },
        { label: "2 baños", value: 2 },
        { label: "3 baños", value: 3 },
        { label: "4 baños", value: 4 },
        { label: "5 baños", value: 5 }
      ],
      kitchenNumber: [
        { label: "1 cocina", value: 1 },
        { label: "2 cocinas", value: 2 },
        { label: "3 cocinas", value: 3 },
        { label: "4 cocinas", value: 4 },
        { label: "5 cocinas", value: 5 }
      ],
      salonNumber: [
        { label: "1 salón", value: 1 },
        { label: "2 salones", value: 2 },
        { label: "3 salones", value: 3 },
        { label: "4 salones", value: 4 },
        { label: "5 salones", value: 5 }
      ],
      pickedFloorType: null,
      pickedFloorNumber: 1,
      pickedRoomNumber: 1,
      pickedBathroomNumber: 1,
      pickedKitchenNumber: 1,
      pickedSalonNumber: 1,
      tasks: [],
      hours: "",
      scrollPage: 0,
      responseData: {},
      finalAddress: "",
      paymentMethod: 1
    };

    this.inputRefs = {};

    this.appraise = this.appraise.bind(this);
    this.backToHome = this.backToHome.bind(this);
    this.onHandleChange = this.onHandleChange.bind(this);
    this.finishAppraise = this.finishAppraise.bind(this);
    this.scrollTo = this.scrollTo.bind(this);
    this.toggleSupplies = this.toggleSupplies.bind(this);
    this.toggleMovility = this.toggleMovility.bind(this);
    this.onRegionChange = this.onRegionChange.bind(this);
    this.showMapModal = this.showMapModal.bind(this);
    this.storeLocation = this.storeLocation.bind(this);
    this.setPaymentMethod = this.setPaymentMethod.bind(this);
  }

  componentDidMount() {
    this.getUserStorage();
    this.setState({ dotsIndex });
  }

  async getUserStorage() {
    let user = await JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ user });
  }

  onHandleChange(quarterMetters) {
    this.setState({ quarterMetters });
  }

  pickedFloorType(floorType) {
    this.setState({
      pickedFloorType: floorType
    });
  }

  pickedFloorNumber(floorNumber) {
    this.setState({
      pickedFloorNumber: floorNumber
    });
  }

  pickedRoomNumber(roomNumber) {
    this.setState({
      pickedRoomNumber: roomNumber
    });
  }

  pickedBathroomNumber(bathroomNumber) {
    this.setState({
      pickedBathroomNumber: bathroomNumber
    });
  }

  pickedKitchenNumber(kitchenNumber) {
    this.setState({
      pickedKitchenNumber: kitchenNumber
    });
  }

  pickedSalonNumber(salonNumber) {
    this.setState({
      pickedSalonNumber: salonNumber
    });
  }

  backToHome() {
    Navigation.pop(this.props.componentId);
  }

  finishAppraise() {
    if (!this.state.quarterMetters.length) {
      Alert.alert(
        "Datos invalidos",
        "Por favor completar los campos requeridos."
      );
      return;
    }
    this.appraise();
  }

  extraTasks(tasks) {
    this.setState({
      tasks
    });
  }

  extraHours(hours) {
    this.setState({
      hours
    });
  }

  storeLocation(region, finalAddress) {
    this.setState({
      region,
      finalAddress,
      storedLocation: true
    });
  }

  showMapModal() {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "clean.Map",
              passProps: {
                storeLocation: this.storeLocation.bind(this)
              },
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                }
              }
            }
          }
        ]
      }
    });
  }

  setPaymentMethod(paymentMethod) {
    this.setState({
      paymentMethod
    });
  }

  async appraise() {
    this.setState({ loader: true });
    const {
      quarterMetters,
      user,
      pickedRoomNumber,
      pickedBathroomNumber,
      pickedKitchenNumber,
      pickedSalonNumber,
      movility,
      supplies,
      hours
    } = this.state;

    const roomObj = {
      ambiente: "Habitacion",
      cantidad_piso: pickedRoomNumber
    };

    const bathroomObj = {
      ambiente: "Baño",
      cantidad_piso: pickedBathroomNumber
    };

    const kitchenObj = {
      ambiente: "Cocina",
      cantidad_piso: pickedKitchenNumber
    };

    const salonObj = {
      ambiente: "Salon",
      cantidad_piso: pickedSalonNumber
    };

    const objAppraise = {
      metrosCuadrados: quarterMetters,
      tipoPiso: JSON.stringify([roomObj, bathroomObj, kitchenObj, salonObj]),
      taxiHabilitado: movility ? 1 : 0,
      horasDisposicion: hours,
      insumosHabilitado: supplies ? 1 : 0,
      user_id: user.id,
      latitud: this.state.region.latitude,
      longitud: this.state.region.longitude,
      comentario: this.state.info,
      tipo_pago: this.state.paymentMethod,
      tipoLimpieza: this.state.tasks
    };

    console.log("objAppraise", objAppraise);

    let response = await fetch(url.appraise, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(objAppraise)
    });
    console.log("response", response);
    let res = await response.json();
    console.log("res", res);

    if (res.statusCode === 200) {
      this.setState({ loader: false, responseData: res.data });
      this.scrollTo(2);
    } else {
      this.setState({ loader: false });
      Alert.alert("Error!", res.message, [{ text: "OK" }]);
    }
  }

  scrollTo(index) {
    this.refs["scroll_appraise"].scrollTo({
      x: width * index,
      y: 0,
      animated: true
    });

    this.setState({
      scrollPage: index
    });
  }

  onRegionChange(region) {
    this.setState({ region });
  }

  toggleSupplies() {
    this.setState({ supplies: !this.state.supplies });
  }

  toggleMovility() {
    this.setState({ movility: !this.state.movility });
  }

  render() {
    const {
      quarterMetters,
      user,
      pickedRoomNumber,
      pickedBathroomNumber,
      pickedKitchenNumber,
      pickedSalonNumber,
      movility,
      supplies,
      hours,
      responseData
    } = this.state;
    return (
      <View
        // source={img.splashBackground}
        style={styles.imageBackgroundContainer}
      >
        {/* <View style={styles.container}> */}
        <View
          style={{
            backgroundColor: "#469CDD",
            width,
            height: height * 0.08,
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: 15,
            flexDirection: "row"
          }}
        >
          <TouchableOpacity
            onPress={() =>
              this.state.scrollPage === 1 || this.props.scrollPage === 2
                ? this.scrollTo(0)
                : Navigation.pop(this.props.componentId)
            }
            style={styles.backButton}
          >
            <Image source={img.backIcon} style={styles.backIcon} />
          </TouchableOpacity>
          <View
            style={{
              width: width * 0.7,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                color: "#fff",
                fontWeight: "bold",
                fontSize: size.normal
              }}
            >
              {this.state.scrollPage === 0
                ? `Información del hogar`
                : `Información adicional `}
            </Text>
          </View>
          <View
            style={{
              width: width * 0.15,
              height: height * 0.1
            }}
          />
        </View>
        <ScrollView
          ref="scroll_appraise"
          scrollEnabled={false}
          horizontal
          showsHorizontalScrollIndicator={false}
        >
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.formContainer}>
              <View
                style={[
                  styles.textInputContainer,
                  {
                    borderColor: this.state.onFocusInput ? "#469CDD" : "#b7b7b7"
                  }
                ]}
              >
                <TextInput
                  keyboardType={"numeric"}
                  value={this.state.quarterMetters}
                  placeholder={"Metros cuadrados"}
                  placeholderTextColor={"#000"}
                  onFocus={() => this.setState({ onFocusInput: true })}
                  onBlur={() => this.setState({ onFocusInput: false })}
                  onChangeText={quarterMetters =>
                    this.onHandleChange(quarterMetters)
                  }
                  style={styles.textInput}
                />
              </View>
              <View style={styles.selectPickerContainer}>
                <RNPickerSelect
                  placeholder={{
                    label: "Cantidad de salas",
                    value: null
                  }}
                  useNativeAndroidPickerStyle={false}
                  items={this.state.roomNumber}
                  onValueChange={value => this.pickedRoomNumber(value)}
                  onUpArrow={() => {
                    this.inputRefs.name.focus();
                  }}
                  onDownArrow={() => {
                    this.inputRefs.picker2.togglePicker();
                  }}
                  placeholderTextColor={"#000"}
                  style={{ ...pickerSelectStyles }}
                  hideIcon
                  value={this.state.pickedRoomNumber}
                  ref={el => {
                    this.inputRefs.picker = el;
                  }}
                />
                <View style={styles.downIconContainer}>
                  <Image source={img.downIcon} style={styles.downIcon} />
                </View>
              </View>
              <View style={styles.selectPickerContainer}>
                <RNPickerSelect
                  placeholder={{
                    label: "Cantidad de baños",
                    value: null
                  }}
                  useNativeAndroidPickerStyle={false}
                  items={this.state.bathroomNumber}
                  onValueChange={value => this.pickedBathroomNumber(value)}
                  onUpArrow={() => {
                    this.inputRefs.name.focus();
                  }}
                  onDownArrow={() => {
                    this.inputRefs.picker2.togglePicker();
                  }}
                  placeholderTextColor={"#000"}
                  style={{ ...pickerSelectStyles }}
                  hideIcon
                  value={this.state.pickedBathroomNumber}
                  ref={el => {
                    this.inputRefs.picker = el;
                  }}
                />
                <View style={styles.downIconContainer}>
                  <Image source={img.downIcon} style={styles.downIcon} />
                </View>
              </View>
              <View style={styles.selectPickerContainer}>
                <RNPickerSelect
                  placeholder={{
                    label: "Cantidad de cocinas",
                    value: null
                  }}
                  useNativeAndroidPickerStyle={false}
                  items={this.state.kitchenNumber}
                  onValueChange={value => this.pickedKitchenNumber(value)}
                  onUpArrow={() => {
                    this.inputRefs.name.focus();
                  }}
                  onDownArrow={() => {
                    this.inputRefs.picker2.togglePicker();
                  }}
                  placeholderTextColor={"#000"}
                  style={{ ...pickerSelectStyles }}
                  hideIcon
                  value={this.state.pickedKitchenNumber}
                  ref={el => {
                    this.inputRefs.picker = el;
                  }}
                />
                <View style={styles.downIconContainer}>
                  <Image source={img.downIcon} style={styles.downIcon} />
                </View>
              </View>
              <View style={styles.selectPickerContainer}>
                <RNPickerSelect
                  placeholder={{
                    label: "Cantidad de salones",
                    value: null
                  }}
                  useNativeAndroidPickerStyle={false}
                  items={this.state.salonNumber}
                  onValueChange={value => this.pickedSalonNumber(value)}
                  onUpArrow={() => {
                    this.inputRefs.name.focus();
                  }}
                  onDownArrow={() => {
                    this.inputRefs.picker2.togglePicker();
                  }}
                  placeholderTextColor={"#000"}
                  style={{ ...pickerSelectStyles }}
                  hideIcon
                  value={this.state.pickedSalonNumber}
                  ref={el => {
                    this.inputRefs.picker = el;
                  }}
                />
                <View style={styles.downIconContainer}>
                  <Image source={img.downIcon} style={styles.downIcon} />
                </View>
              </View>
              <ExtraTasks extraTasks={this.extraTasks.bind(this)} />
            </View>
            <View
              style={{
                width,
                height: height * 0.12,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onPress={() => this.scrollTo(1)}
                activeOpacity={0.8}
                style={{
                  width: width * 0.5,
                  height: height * 0.065,
                  backgroundColor: "#469CDD",
                  justifyContent: "center",
                  alignItems: "center",
                  borderWidth: 1,
                  borderColor: "#fff",
                  borderRadius: 5
                }}
              >
                <Text style={{ color: "#fff" }}>Continuar</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View
              style={{
                width: width * 0.9,
                alignSelf: "center",
                height: height * 0.05
              }}
            >
              <Text style={{ fontSize: size.regular }}>
                ¿Para cuándo lo necesitas?
              </Text>
            </View>
            <ExtraHours extraHours={this.extraHours.bind(this)} />
            <TouchableOpacity
              onPress={this.showMapModal}
              style={{
                width: width * 0.9,
                height: height * 0.06,
                borderRadius: 5,
                alignSelf: "center",
                marginBottom: 20,
                borderWidth: this.state.storedLocation ? 1.5 : 1,
                borderColor: this.state.storedLocation ? "#469CDD" : "#b7b7b7",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: this.state.storedLocation ? "#469CDD" : "grey",
                  fontWeight: this.state.storedLocation ? "bold" : "100"
                }}
              >
                {this.state.storedLocation
                  ? "Ubicación guardada"
                  : "Ubicar en el mapa"}
              </Text>
            </TouchableOpacity>
            <View
              style={{
                width,
                height: height * 0.36
              }}
            >
              <View
                style={{
                  width: width * 0.9,
                  paddingLeft: 20
                }}
              >
                <Text> Dirección escogida en el mapa </Text>
              </View>
              <View
                style={{
                  marginTop: 10,
                  width: width * 0.9,
                  height: height * 0.06,
                  borderWidth: 1,
                  borderColor: "#b7b7b7",
                  borderRadius: 5,
                  alignSelf: "center",
                  justifyContent: "flex-start"
                }}
              >
                <TextInput
                  onChangeText={info => this.setState({ finalAddress: info })}
                  value={this.state.finalAddress}
                  placeholder={"Dirección"}
                  placeholderTextColor={"#b7b7b7"}
                  style={{
                    width: width * 0.9,
                    height: height * 0.06,
                    paddingLeft: 10,
                    textAlignVertical: "top"
                  }}
                />
              </View>
              <PaymentMethod setPaymentMethod={this.setPaymentMethod} />
              <View
                style={{
                  width: width * 0.9,
                  height: height * 0.1,
                  justifyContent: "space-evenly",
                  alignItems: "center",
                  flexDirection: "row",
                  paddingLeft: 20
                }}
              >
                <View style={styles.centerToggleSwitch}>
                  <View
                    style={[
                      styles.colorOff,
                      this.state.supplies && styles.colorOn
                    ]}
                  >
                    <TouchableOpacity
                      activeOpacity={0.9}
                      onPress={this.toggleSupplies}
                      style={[
                        styles.justifyContentStart,
                        this.state.supplies && styles.justifyContentEnd
                      ]}
                    >
                      <View style={styles.switchToggleContent} />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity
                  onPress={this.toggleSupplies}
                  activeOpacity={0.8}
                  style={{ width: width * 0.7 }}
                >
                  <Text style={styles.configSubtitleText}>
                    El profesional traerá los productos de limpieza necesarios
                    (+ S/. 20.00 )
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                width,
                height: height * 0.1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onPress={this.finishAppraise}
                activeOpacity={0.8}
                style={{
                  width: width * 0.5,
                  height: height * 0.065,
                  backgroundColor: "#469CDD",
                  justifyContent: "center",
                  alignItems: "center",
                  borderWidth: 1,
                  borderColor: "#fff",
                  borderRadius: 5
                }}
              >
                {this.state.loader ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  <Text style={{ color: "#fff" }}>Cotizar</Text>
                )}
              </TouchableOpacity>
            </View>
          </ScrollView>
          {/* <ScrollView showsVerticalScrollIndicator={false}>
            <View>

            </View>
          </ScrollView> */}
          <ScrollView showsVerticalScrollIndicator={false}>
            <Resumen
              quarterMetters={quarterMetters}
              user={user}
              roomNumber={pickedRoomNumber}
              bathroomNumber={pickedBathroomNumber}
              kitchenNumber={pickedKitchenNumber}
              salonNumber={pickedSalonNumber}
              movility={movility}
              supplies={supplies}
              hours={hours}
              precioFinal={responseData.precioFinal || 0}
              componentId={this.props.componentId}
            />
          </ScrollView>
        </ScrollView>
        {/* </View> */}
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.3)"
  },
  imageBackgroundContainer: {
    flex: 1,
    backgroundColor: "#fff"
  },
  backButton: {
    width: width * 0.15,
    height: height * 0.1,
    justifyContent: "center"
  },
  backIcon: {
    tintColor: "#fff",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  },
  formContainer: {
    alignItems: "center"
  },
  textInputContainer: {
    width: width * 0.8,
    height: height * 0.06,
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: 10,
    marginVertical: 5
  },
  textInput: {
    height: height * 0.06,
    color: "#000"
  },
  selectPickerContainer: {
    width: width * 0.8,
    height: height * 0.06,
    borderWidth: 1,
    borderColor: "#b7b7b7",
    borderRadius: 5,
    marginVertical: 5,
    flexDirection: "row"
  },
  downIconContainer: {
    width: width * 0.1,
    justifyContent: "center",
    alignItems: "center"
  },
  downIcon: {
    tintColor: "#b7b7b7",
    resizeMode: "contain",
    width: width * 0.03
  },
  centerToggleSwitch: {
    width: width * 0.15
  },
  setupContainer: {
    backgroundColor: "#fff",
    width: width * 0.9,
    height: height * 0.25,
    alignSelf: "center",
    justifyContent: "space-evenly",
    borderRadius: 10,
    flexDirection: "column"
  },
  switchToggleContent: {
    backgroundColor: "#fff",
    height: 25,
    width: 25,
    borderWidth: 1,
    borderColor: "#b7b7b7",
    borderRadius: 15,
    zIndex: 5000
  },
  colorOff: {
    backgroundColor: "#b9b7b8",
    width: 40,
    height: 25,
    borderRadius: 25
  },
  colorOn: {
    backgroundColor: "#469CDD"
  },
  justifyContentStart: {
    width: 40,
    height: 25,
    alignItems: "center",
    flexDirection: "row"
  },
  justifyContentEnd: {
    justifyContent: "flex-end"
  },
  configSubtitleText: {
    color: "#000",
    fontSize: size.exact
  }
});

const pickerSelectStyles = StyleSheet.create({
  inputAndroid: {
    color: "#000",
    width: width * 0.7,
    paddingLeft: 10
  },
  inputIOS: {
    fontSize: 16,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderRadius: 4,
    color: "#fff"
  }
});
