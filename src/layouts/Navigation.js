import { Navigation } from "react-native-navigation";

export const goHome = () =>
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              id: "Home",
              name: "clean.Home"
            }
          }
        ]
      }
    }
  });

export const goToAuth = () =>
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              id: "Login",
              name: "clean.Login"
            }
          }
        ]
      }
    }
  });
