import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  AsyncStorage,
  ActivityIndicator,
  Alert
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import RNPickerSelect from "react-native-picker-select";
import moment from "moment";
import constants from "../config/constants";
import url from "../config/url";
import { goHome } from "./Navigation";

const { width, height, size } = constants;

export default class Update extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
      onFocusNameInput: false,
      onFocusLastnameInput: false,
      onFocusBirthdatePicker: false,
      isDateTimePickerVisible: false,
      name: "",
      lastname: "",
      dateBirth: "",
      gender: [
        { label: "Masculino", value: "M" },
        { label: "Femenino", value: "F" }
      ],
      pickedGender: null,
      loader: false,
      user_social: null
    };

    this.inputRefs = {};
    this._showDateTimePicker = this._showDateTimePicker.bind(this);
    this._hideDateTimePicker = this._hideDateTimePicker.bind(this);
    this._handleDatePicked = this._handleDatePicked.bind(this);
    this.handleActive = this.handleActive.bind(this);
    this.activeGender = this.activeGender.bind(this);
    this.pickedGender = this.pickedGender.bind(this);
    this.updateUserInfo = this.updateUserInfo.bind(this);
  }

  componentDidMount() {
    this.getUserStorage();
  }

  updateUserInfo() {
    this.setState({ loader: true });
    if (
      !this.state.name.length ||
      !this.state.lastname.length ||
      !this.state.pickedGender ||
      !this.state.dateBirth
    ) {
      this.setState({ loader: false });
      Alert.alert(
        "Datos invalidos",
        "Por favor completar los campos requeridos."
      );
      return;
    }
    this.update();
  }

  async update() {
    this.setState({
      loader: true
    });

    const objProfile = {
      user_id: this.state.user.id,
      name: this.state.name,
      last_name: this.state.lastname,
      date_birthday: this.state.dateBirth,
      gender: this.state.pickedGender
    };

    const storageObjProfile = {
      id: this.state.user.id,
      name: this.state.name,
      last_name: this.state.lastname,
      date_birthday: this.state.dateBirth,
      gender: this.state.pickedGender
    };

    let response = await fetch(url.updateProfile, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(objProfile)
    });
    let res = await response.json();
    if (res.statusCode === 200) {
      AsyncStorage.setItem("user", JSON.stringify(storageObjProfile));
      this.setState({ loader: false });
      goHome();
    } else {
      this.setState({ loader: false });
      Alert.alert("Error!", res.message, [{ text: "OK" }]);
    }
  }

  pickedGender(pickedGender) {
    this.setState({
      pickedGender
    });
  }

  async getUserStorage() {
    let user = await JSON.parse(await AsyncStorage.getItem("user"));
    let user_social = await JSON.parse(
      await AsyncStorage.getItem("user_social")
    );
    if (user_social) {
      this.setState({
        user,
        user_social,
        name: user_social.first_name,
        lastname: user_social.last_name
      });
    } else {
      this.setState({
        user
      });
    }
  }

  handleActive(activeGender) {
    this.setState({
      activeGender
    });
  }

  activeGender(id) {
    return id === this.state.activeGender;
  }

  _showDateTimePicker() {
    this.setState({ isDateTimePickerVisible: true });
  }

  _hideDateTimePicker() {
    this.setState({ isDateTimePickerVisible: false });
  }

  _handleDatePicked(date) {
    this.setState(
      {
        dateBirth: moment(new Date(date)).format("YYYY-MM-DD")
      },
      () => this._hideDateTimePicker()
    );
  }

  render() {
    return (
      <View style={styles.imageBackgroundContainer}>
        <View
          style={{
            width,
            height: height * 0.1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text
            style={{
              color: "#469CDD",
              fontSize: size.normal,
              fontWeight: "300"
            }}
          >
            {" "}
            Actualiza tu información para continuar{" "}
          </Text>
        </View>
        <View
          style={{
            marginTop: 10,
            width: width * 0.9,
            height: height * 0.06,
            borderWidth: 1,
            borderColor: this.state.onFocusNameInput ? "#469CDD" : "#b7b7b7",
            borderRadius: 5,
            alignSelf: "center",
            justifyContent: "flex-start",
            marginBottom: 8
          }}
        >
          <TextInput
            onFocus={() => this.setState({ onFocusNameInput: true })}
            onBlur={() => this.setState({ onFocusNameInput: false })}
            onChangeText={name => this.setState({ name })}
            value={this.state.name}
            placeholder={"Nombres"}
            placeholderTextColor={"#b7b7b7"}
            style={{
              width: width * 0.9,
              height: height * 0.06,
              paddingLeft: 10,
              textAlignVertical: "top"
            }}
          />
        </View>
        <View
          style={{
            marginTop: 10,
            width: width * 0.9,
            height: height * 0.06,
            borderWidth: 1,
            borderColor: this.state.onFocusLastnameInput
              ? "#469CDD"
              : "#b7b7b7",
            borderRadius: 5,
            alignSelf: "center",
            justifyContent: "flex-start",
            marginBottom: 8
          }}
        >
          <TextInput
            onFocus={() => this.setState({ onFocusLastnameInput: true })}
            onBlur={() => this.setState({ onFocusLastnameInput: false })}
            onChangeText={lastname => this.setState({ lastname })}
            value={this.state.lastname}
            placeholder={"Apellidos"}
            placeholderTextColor={"#b7b7b7"}
            style={{
              width: width * 0.9,
              height: height * 0.06,
              paddingLeft: 10,
              textAlignVertical: "top"
            }}
          />
        </View>
        <TouchableOpacity
          onPress={this._showDateTimePicker}
          onFocus={() => this.setState({ onFocusBirthdatePicker: true })}
          onBlur={() => this.setState({ onFocusLastnameInput: false })}
          activeOpacity={0.9}
          style={{
            marginTop: 10,
            width: width * 0.9,
            height: height * 0.06,
            borderWidth: 1,
            borderColor: this.state.onFocusBirthdatePicker
              ? "#469CDD"
              : "#b7b7b7",
            borderRadius: 5,
            alignSelf: "center",
            justifyContent: "center",
            marginBottom: 8
          }}
        >
          <Text
            style={{
              color: this.state.dateBirth ? "#000" : "#b7b7b7",
              paddingLeft: 10
            }}
          >
            {this.state.dateBirth || "Fecha de nacimiento"}
          </Text>
        </TouchableOpacity>
        <View
          style={{
            marginTop: 10,
            width: width * 0.9,
            height: height * 0.06,
            borderWidth: 1,
            borderColor: "#b7b7b7",
            borderRadius: 5,
            alignSelf: "center",
            justifyContent: "flex-start",
            marginBottom: 8
          }}
        >
          <RNPickerSelect
            placeholder={{
              label: "Genero",
              value: null
            }}
            useNativeAndroidPickerStyle={false}
            items={this.state.gender}
            onValueChange={value => this.pickedGender(value)}
            onUpArrow={() => {
              this.inputRefs.name.focus();
            }}
            onDownArrow={() => {
              this.inputRefs.picker2.togglePicker();
            }}
            placeholderTextColor={"#b7b7b7"}
            style={{ ...pickerSelectStyles }}
            hideIcon
            value={this.state.pickedGender}
            ref={el => {
              this.inputRefs.picker = el;
            }}
          />
        </View>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
        />
        <View
          style={{
            width,
            height: height * 0.42,
            justifyContent: "flex-end",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            onPress={this.updateUserInfo}
            style={{
              width: width * 0.7,
              height: height * 0.08,
              backgroundColor: "#469CDD",
              borderRadius: 5,
              borderWidth: 1,
              borderColor: "#fff",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            {this.state.loader ? (
              <ActivityIndicator size="large" color="#fff" />
            ) : (
              <Text
                style={{
                  fontSize: size.normal,
                  fontWeight: "300",
                  textAlign: "center",
                  color: "#fff"
                }}
              >
                Actualizar
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  imageBackgroundContainer: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    paddingTop: 15
  },
  backButton: {
    width: width * 0.15,
    height: height * 0.1,
    justifyContent: "center"
  },
  backIcon: {
    tintColor: "#fff",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  },
  formContainer: {
    alignItems: "center"
  },
  textInputContainer: {
    width: width * 0.8,
    height: height * 0.06,
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: 10,
    marginVertical: 5
  },
  textInput: {
    height: height * 0.06,
    color: "#000"
  },
  centerToggleSwitch: {
    width: width * 0.15
  },
  setupContainer: {
    backgroundColor: "#fff",
    width: width * 0.9,
    height: height * 0.25,
    alignSelf: "center",
    justifyContent: "space-evenly",
    borderRadius: 10,
    flexDirection: "column"
  },
  switchToggleContent: {
    backgroundColor: "#fff",
    height: 25,
    width: 25,
    borderWidth: 1,
    borderColor: "#b7b7b7",
    borderRadius: 15,
    zIndex: 5000
  },
  colorOff: {
    backgroundColor: "#b9b7b8",
    width: 40,
    height: 25,
    borderRadius: 25
  },
  colorOn: {
    backgroundColor: "#469CDD"
  },
  justifyContentStart: {
    width: 40,
    height: 25,
    alignItems: "center",
    flexDirection: "row"
  },
  justifyContentEnd: {
    justifyContent: "flex-end"
  },
  configSubtitleText: {
    color: "#000",
    fontSize: size.exact
  }
});

const pickerSelectStyles = StyleSheet.create({
  inputAndroid: {
    color: "#000",
    width: width * 0.7,
    paddingLeft: 10
  },
  inputIOS: {
    fontSize: 16,
    paddingTop: 13,
    paddingHorizontal: 10,
    paddingBottom: 12,
    borderRadius: 4,
    color: "#fff"
  }
});
