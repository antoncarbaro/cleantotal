import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity, Image, Text } from "react-native";
import { Navigation } from "react-native-navigation";
import Geocoder from "react-native-geocoding";
import MapView, { Marker } from "react-native-maps";

import constants from "../config/constants";
import img from "../config/images";

const { height, width, dotsIndex, size } = constants;

Geocoder.init("AIzaSyCyvS4nc_RNCdwABwu3-DwO6tO9BTJe73I");

export default class Map extends Component {
  constructor(props) {
    super(props);
    this.navigationEventListener = Navigation.events().bindComponent(this);
    this.state = {
      region: {
        latitude: -12.121828,
        longitude: -77.0327747,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      finalAddress: "",
      showActive: false
    };
    this.onRegionChange = this.onRegionChange.bind(this);
    this.dismissModal = this.dismissModal.bind(this);
    this.saveLocation = this.saveLocation.bind(this);
  }

  componentDidMount() {
    this.getAddressName(
      this.state.region.latitude,
      this.state.region.longitude
    );
  }

  onRegionChange(region) {
    this.getAddressName(region.latitude, region.longitude);
    this.setState({ region, showActive: true });
  }

  getAddressName(latitude, longitude) {
    Geocoder.from(latitude, longitude)
      .then(json => {
        let addressNumber = json.results[0].address_components[0].short_name;
        let address = json.results[0].address_components[1].short_name;
        let district = json.results[0].address_components[2].short_name;
        let finalAddress = `${address} ${addressNumber}, ${district}`;
        this.setState({
          finalAddress
        });
      })
      .catch(error => console.warn(error));
  }

  dismissModal() {
    Navigation.dismissModal(this.props.componentId);
  }

  saveLocation() {
    this.props.storeLocation(this.state.region, this.state.finalAddress);
    this.setState({
      finalAddress: "",
      showActive: false
    });
    this.dismissModal();
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            width,
            height: height * 0.1,
            justifyContent: "center",
            zIndex: 400000,
            position: "absolute",
            backgroundColor: "rgba(0,0,0,0.1)"
          }}
        >
          <TouchableOpacity
            onPress={this.dismissModal}
            style={styles.backButton}
          >
            <Image source={img.backIcon} style={styles.backIcon} />
          </TouchableOpacity>
        </View>
        <MapView
          initialRegion={this.state.region}
          loadingEnabled={true}
          loadingIndicatorColor={"#469CDD"}
          moveOnMarkerPress={false}
          showsUserLocation={true}
          followsUserLocation={true}
          rotateEnabled={false}
          pitchEnabled={false}
          onRegionChangeComplete={this.onRegionChange}
          style={styles.map}
        >
          <Marker coordinate={this.state.region} />
        </MapView>
        {this.state.showActive && (
          <View style={styles.activeButton}>
            <View
              style={{
                width,
                height: height * 0.07,
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: size.normal,
                  textAlign: "center"
                }}
              >
                {this.state.finalAddress}
              </Text>
            </View>
            <TouchableOpacity
              onPress={this.saveLocation}
              activeOpacity={0.8}
              style={{
                backgroundColor: "#469CDD",
                width: width * 0.5,
                height: height * 0.06,
                borderRadius: 5,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text style={{ color: "#fff", fontSize: size.regular }}>
                Confirmar ubicación
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.35)"
  },
  map: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.35)"
  },
  backIcon: {
    tintColor: "#469CDD",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  },
  backButton: {
    width: width * 0.15,
    height: height * 0.1,
    justifyContent: "center"
  },
  activeButton: {
    position: "absolute",
    backgroundColor: "rgba(0,0,0,0.35)",
    width,
    height: height * 0.2,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center"
  }
});
