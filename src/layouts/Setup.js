import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  AsyncStorage,
  TextInput,
  Alert,
  ActivityIndicator
} from "react-native";
import { Navigation } from "react-native-navigation";
import { goToAuth } from "./Navigation";

import img from "../config/images";

import constants from "../config/constants";
import url from "../config/url";

const { height, width, size } = constants;

export default class Setup extends Component {
  constructor(props) {
    super(props);
    this.navigationEventListener = Navigation.events().bindComponent(this);
    this.state = {
      user: {},
      nameOnFocusInput: false,
      lastameOnFocusInput: false,
      name: "",
      lastname: "",
      loader: false
    };
    this.onHandleChange = this.onHandleChange.bind(this);
    this.updateInfo = this.updateInfo.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidAppear() {
    this.getUserStorage();
  }

  async getUserStorage() {
    let user = await JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ user });
  }

  async logout() {
    await AsyncStorage.removeItem("user");
    goToAuth();
  }

  onHandleChange(value, nameState) {
    this.setState({ [nameState]: value });
  }

  async updateInfo() {
    this.setState({
      loader: true
    });

    const objProfile = {
      user_id: this.state.user.id,
      name: this.state.name,
      last_name: this.state.last_name
    };

    let response = await fetch(url.updateProfile, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(objProfile)
    });
    let res = await response.json();
    if (res.statusCode === 200) {
      this.setState({ loader: false });
    } else {
      this.setState({ loader: false });
      Alert.alert("Error!", res.message, [{ text: "OK" }]);
    }
  }

  render() {
    const {
      user: { gender }
    } = this.state;
    const user_social = this.state.user_social || null;
    return (
      <View style={{ width, height }}>
        <View
          style={{
            backgroundColor: "#469CDD",
            width,
            height: height * 0.08,
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: 15,
            flexDirection: "row"
          }}
        >
          <TouchableOpacity
            onPress={() => Navigation.pop(this.props.componentId)}
            style={styles.backButton}
          >
            <Image source={img.backIcon} style={styles.backIcon} />
          </TouchableOpacity>
          <View
            style={{
              width: width * 0.7,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                color: "#fff",
                fontWeight: "bold",
                fontSize: size.normal
              }}
            >
              Configuración
            </Text>
          </View>
          <View
            style={{
              width: width * 0.2,
              height: height * 0.1
            }}
          />
        </View>
        <View style={styles.userContainer}>
          <View style={styles.imageContainer}>
            <View
              style={{
                width: width * 0.365,
                height: height * 0.25
              }}
            />
            <View style={styles.profileBorder}>
              {!user_social ? (
                <Image
                  source={gender === "M" ? img.maleUser : img.femaleUser}
                  style={styles.profileImage}
                />
              ) : (
                <Image
                  source={{ uri: user_social.picture.data.url }}
                  style={styles.profileImage}
                />
              )}
            </View>
            <View style={styles.logoutContainer}>
              <TouchableOpacity activeOpacity={0.8} onPress={this.logout}>
                <Image source={img.powerIcon} style={styles.logoutIcon} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.usernameContainer}>
            <Text style={styles.usernameText}>{this.state.user.email}</Text>
          </View>
        </View>
        <View
          style={{
            width,
            height: height * 0.18,
            justifyContent: "space-evenly",
            alignItems: "center"
          }}
        >
          <View
            style={[
              styles.textInputContainer,
              {
                borderColor: this.state.nameOnFocusInput ? "#469CDD" : "#b7b7b7"
              }
            ]}
          >
            <TextInput
              value={this.state.name}
              placeholder={"Nombre"}
              placeholderTextColor={"grey"}
              onFocus={() => this.setState({ nameOnFocusInput: true })}
              onBlur={() => this.setState({ nameOnFocusInput: false })}
              onChangeText={name => this.onHandleChange(name, "name")}
              style={styles.textInput}
            />
          </View>
          <View
            style={[
              styles.textInputContainer,
              {
                borderColor: this.state.lastameOnFocusInput
                  ? "#469CDD"
                  : "#b7b7b7"
              }
            ]}
          >
            <TextInput
              value={this.state.lastname}
              placeholder={"Apellido"}
              placeholderTextColor={"grey"}
              onFocus={() => this.setState({ lastameOnFocusInput: true })}
              onBlur={() => this.setState({ lastameOnFocusInput: false })}
              onChangeText={lastname =>
                this.onHandleChange(lastname, "lastname")
              }
              style={styles.textInput}
            />
          </View>
        </View>
        <View
          style={{
            width,
            height: height * 0.4,
            justifyContent: "flex-end",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            onPress={this.updateInfo}
            style={styles.appraiseButton}
          >
            {this.state.loader ? (
              <ActivityIndicator size="small" color="#fff" />
            ) : (
              <Text style={{ color: "#fff" }}>Actualizar</Text>
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backButton: {
    width: width * 0.15,
    height: height * 0.1,
    justifyContent: "center"
  },
  backIcon: {
    tintColor: "#fff",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  },
  userContainer: {
    width,
    height: height * 0.25,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column"
  },
  imageContainer: {
    width,
    height: height * 0.25,
    flexDirection: "row",
    alignItems: "flex-end"
  },
  profileBorder: {
    borderRadius: 50,
    width: 103,
    height: 103,
    justifyContent: "center",
    alignItems: "center"
  },
  profileImage: {
    height: 97.5,
    width: 97.5,
    borderRadius: 50,
    resizeMode: "cover"
  },
  usernameContainer: {
    width,
    height: height * 0.1,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  usernameText: {
    fontSize: size.normal,
    color: "#000",
    fontWeight: "300"
  },
  logoutContainer: {
    width: width * 0.365,
    height: height * 0.25,
    justifyContent: "center",
    alignItems: "center"
  },
  logoutIcon: {
    resizeMode: "contain",
    width: width * 0.1,
    height: height * 0.05,
    tintColor: "#acd7ef"
  },
  textInputContainer: {
    width: width * 0.8,
    height: height * 0.06,
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: 10,
    marginVertical: 5
  },
  textInput: {
    height: height * 0.06,
    color: "#000"
  },
  appraiseButton: {
    width: width * 0.6,
    height: height * 0.065,
    backgroundColor: "#469CDD",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  }
});
