import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  ActivityIndicator,
  Alert,
  AsyncStorage
} from "react-native";
import { Navigation } from "react-native-navigation";
import FlipComponent from "react-native-flip-component";

import constants from "../config/constants";
import img from "../config/images";
import url from "../config/url";

const { culqiUrl } = url;
const { height, width, size, publicKeyCulqi } = constants;

export default class Payment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardNumber: "",
      cardMonth: "",
      cardYear: "",
      cardCcv: "",
      cardName: "",
      focusNumber: "",
      focusMonth: "",
      focusYear: "",
      focusName: "",
      focusCcv: "",
      isFlipped: false,
      loader: false,
      user: {}
    };

    this.backToHome = this.backToHome.bind(this);
    this.finishPayment = this.finishPayment.bind(this);
  }

  componentDidMount() {
    this.getUserStorage();
    this.setState({
      isFlipped: true
    });
  }

  async getUserStorage() {
    let user = await JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({
      user
    });
  }

  backToHome() {
    Navigation.pop(this.props.componentId);
  }

  finishPayment() {
    if (
      !this.state.cardNumber.length ||
      !this.state.cardMonth.length ||
      !this.state.cardYear.length ||
      !this.state.cardCcv.length ||
      !this.state.cardName.length
    ) {
      Alert.alert(
        "Datos invalidos",
        "Por favor completar los campos requeridos."
      );
      return;
    }

    this.payment();
  }

  async payment() {
    // const objPayment = {
    //   card_number: this.state.cardNumber,
    //   cvv: this.state.cardCcv,
    //   expiration_month: this.state.cardMonth,
    //   expiration_year: this.state.cardYear,
    //   email: this.state.user.email
    // };
    // const authorization = `Bearer ${publicKeyCulqi}`;
    // let responsePayment = await fetch(culqiUrl, {
    //   method: "POST",
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json",
    //     Authorization: authorization
    //   },
    //   body: JSON.stringify(objPayment)
    // });
    // let res = await responsePayment.json();

    Alert.alert("Listo", "Su pago se realizo con exito.");
  }

  render() {
    return (
      <View style={styles.imageBackgroundContainer}>
        <View style={styles.topContainer}>
          <TouchableOpacity onPress={this.backToHome} style={styles.backButton}>
            <Image source={img.backIcon} style={styles.backIcon} />
          </TouchableOpacity>
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.formContainer}>
            <FlipComponent
              isFlipped={this.state.isFlipped}
              rotateDuration={200}
              frontView={
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: "#b7b7b7",
                    borderRadius: 5,
                    width: width * 0.8,
                    height: height * 0.3,
                    backgroundColor: "#fff"
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "grey",
                      width: width * 0.8,
                      height: height * 0.06,
                      marginTop: height * 0.03
                    }}
                  />
                  <View
                    style={{
                      width: width * 0.8,
                      height: height * 0.22,
                      justifyContent: "center",
                      alignItems: "center",
                      backgroundColor: "#fff"
                    }}
                  >
                    <View
                      style={{
                        width: width * 0.8,
                        height: height * 0.08,
                        justifyContent: "center",
                        paddingLeft: 20
                      }}
                    >
                      <Text
                        style={{ color: "#b7b7b7", fontSize: size.regular }}
                      >
                        {this.state.cardCcv || "Ccv"}
                      </Text>
                    </View>
                  </View>
                </View>
              }
              backView={
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: "#b7b7b7",
                    borderRadius: 5,
                    width: width * 0.8,
                    height: height * 0.3,
                    backgroundColor: "#fff"
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "grey",
                      width: width * 0.8,
                      height: height * 0.06,
                      marginTop: height * 0.03
                    }}
                  />
                  <View
                    style={{
                      width: width * 0.8,
                      height: height * 0.08,
                      padding: 20,
                      justifyContent: "center"
                    }}
                  >
                    <Text style={{ color: "#b7b7b7", fontSize: size.regular }}>
                      {this.state.cardNumber ||
                        "* * * *    * * * *   * * * *   * * * *"}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: width * 0.7,
                      height: height * 0.05,
                      justifyContent: "center",
                      alignItems: "flex-end"
                    }}
                  >
                    <Text style={{ color: "#b7b7b7", fontSize: size.regular }}>
                      {this.state.cardMonth
                        ? `${this.state.cardMonth} / ${this.state.cardYear}`
                        : "Mes / Año"}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: width * 0.8,
                      height: height * 0.05,
                      justifyContent: "flex-end",
                      paddingLeft: 15
                    }}
                  >
                    <Text style={{ color: "#b7b7b7", fontSize: size.regular }}>
                      {this.state.cardName ||
                        "Nombre del titular de la tarjeta"}
                    </Text>
                  </View>
                </View>
              }
            />
            <View
              style={{
                width: width * 0.9,
                height: height * 0.41,
                alignItems: "center",
                paddingTop: height * 0.05,
                justifyContent: "space-around"
              }}
            >
              <TextInput
                keyboardType={"numeric"}
                value={this.state.cardNumber}
                placeholder={"Número de tarjeta"}
                placeholderTextColor={"#b7b7b7"}
                onChangeText={cardNumber => this.setState({ cardNumber })}
                onFocus={() => this.setState({ focusNumber: true })}
                onBlur={() => this.setState({ focusNumber: false })}
                style={{
                  borderWidth: 1,
                  borderColor: this.state.focusNumber ? "#469CDD" : "#b7b7b7",
                  width: width * 0.8,
                  borderRadius: 5,
                  paddingLeft: 10
                }}
              />
              <View
                style={{
                  flexDirection: "row",
                  width: width * 0.8,
                  justifyContent: "space-between"
                }}
              >
                <TextInput
                  keyboardType={"numeric"}
                  value={this.state.cardMonth}
                  placeholder={"Mes"}
                  placeholderTextColor={"#b7b7b7"}
                  onChangeText={cardMonth => this.setState({ cardMonth })}
                  onFocus={() => this.setState({ focusMonth: true })}
                  onBlur={() => this.setState({ focusMonth: false })}
                  style={{
                    borderWidth: 1,
                    borderColor: this.state.focusMonth ? "#469CDD" : "#b7b7b7",
                    width: width * 0.32,
                    borderRadius: 5,
                    paddingLeft: 10
                  }}
                />
                <TextInput
                  keyboardType={"numeric"}
                  value={this.state.cardYear}
                  placeholder={"Año"}
                  placeholderTextColor={"#b7b7b7"}
                  onChangeText={cardYear => this.setState({ cardYear })}
                  onFocus={() => this.setState({ focusYear: true })}
                  onBlur={() => this.setState({ focusYear: false })}
                  style={{
                    borderWidth: 1,
                    borderColor: this.state.focusYear ? "#469CDD" : "#b7b7b7",
                    width: width * 0.32,
                    borderRadius: 5,
                    paddingLeft: 10
                  }}
                />
              </View>
              <TextInput
                value={this.state.cardName}
                placeholder={"Titular de la tarjeta"}
                placeholderTextColor={"#b7b7b7"}
                onChangeText={cardName => this.setState({ cardName })}
                onFocus={() => this.setState({ focusName: true })}
                onBlur={() => this.setState({ focusName: false })}
                style={{
                  borderWidth: 1,
                  borderColor: this.state.focusName ? "#469CDD" : "#b7b7b7",
                  width: width * 0.8,
                  borderRadius: 5,
                  paddingLeft: 10
                }}
              />
              <TextInput
                keyboardType={"numeric"}
                value={this.state.cardCcv}
                placeholder={"Ccv"}
                placeholderTextColor={"#b7b7b7"}
                onChangeText={cardCcv => this.setState({ cardCcv })}
                onFocus={() =>
                  this.setState({
                    focusCcv: true,
                    isFlipped: !this.state.isFlipped
                  })
                }
                onBlur={() =>
                  this.setState({
                    focusCcv: false,
                    isFlipped: !this.state.isFlipped
                  })
                }
                style={{
                  borderWidth: 1,
                  borderColor: this.state.focusCcv ? "#469CDD" : "#b7b7b7",
                  width: width * 0.8,
                  borderRadius: 5,
                  paddingLeft: 10
                }}
              />
            </View>
            <View
              style={{
                width,
                height: height * 0.15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onPress={this.finishPayment}
                activeOpacity={0.8}
                style={{
                  width: width * 0.5,
                  height: height * 0.065,
                  backgroundColor: "#469CDD",
                  justifyContent: "center",
                  alignItems: "center",
                  borderWidth: 1,
                  borderColor: "#fff",
                  borderRadius: 5
                }}
              >
                {this.state.loader ? (
                  <ActivityIndicator size="small" color="#fff" />
                ) : (
                  <Text style={{ color: "#fff" }}>Cotizar</Text>
                )}
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.3)"
  },
  imageBackgroundContainer: {
    flex: 1,
    backgroundColor: "#fff"
  },
  backButton: {
    width,
    height: height * 0.1,
    justifyContent: "center"
  },
  backIcon: {
    tintColor: "#fff",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  },
  topContainer: {
    backgroundColor: "#469CDD",
    width,
    height: height * 0.08,
    justifyContent: "center",
    marginBottom: 15
  },
  formContainer: {
    alignItems: "center"
  }
});
