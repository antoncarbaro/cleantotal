import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import { Navigation } from "react-native-navigation";
import MyListItem from "../components/MyListItem";

import url from "../config/url";
import img from "../config/images";
import constants from "../config/constants";

const { height, width, size } = constants;
const { getAppraise } = url;

export default class List extends Component {
  constructor(props) {
    super(props);
    this.navigationEventListener = Navigation.events().bindComponent(this);
    this.state = {
      appraiseList: [],
      loader: false
    };

    this._keyExtractor = this._keyExtractor.bind(this);
    this._renderItem = this._renderItem.bind(this);
    this._onPressItem = this._onPressItem.bind(this);
  }

  componentDidAppear() {
    this.getUserStorage();
  }

  async getUserStorage() {
    let user = await JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ user }, () => this.getAppraises());
  }

  async getAppraises() {
    this.setState({ loader: true });
    const getResponse = await fetch(
      `${getAppraise}?user_id=${this.state.user.id}`
    );
    let response = await getResponse.json();
    if (response.statusCode === 200) {
      console.log(response);
      this.setState({
        appraiseList: response.data,
        loader: false
      });
    } else {
      this.setState({
        loader: false
      });
    }
  }

  _keyExtractor(item) {
    return "" + item.id;
  }

  _onPressItem(id) {
    Navigation.push(this.props.componentId, {
      component: {
        name: "clean.PaymentView",
        passProps: {
          appraiseId: id
        }
      }
    });
  }

  _renderItem({ item }) {
    return (
      <MyListItem
        componentId={this.props.componentId}
        id={item.id}
        onPressItem={this._onPressItem}
        comentario={item.comentario}
        id_user={item.id_user}
        latitud={item.latitud}
        longitud={item.longitud}
        metros_cuadrados={item.metros_cuadrados}
        precio_final={item.precio_final}
        precio_mt2={item.precio_mt2}
        status={item.status}
        total_limpiar={item.total_limpiar}
        tipo_piso={item.tipo_piso}
        fecha_creacion={item.created_at}
      />
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: "#469CDD",
            width,
            height: height * 0.08,
            justifyContent: "center"
          }}
        >
          <TouchableOpacity
            onPress={() => Navigation.pop(this.props.componentId)}
            style={styles.backButton}
          >
            <Image source={img.backIcon} style={styles.backIcon} />
          </TouchableOpacity>
        </View>
        <View style={styles.list}>
          {this.state.loader ? (
            <View style={styles.loaderContainer}>
              <ActivityIndicator size="large" color="#469CDD" />
            </View>
          ) : this.state.appraiseList.length ? (
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.appraiseList}
              renderItem={this._renderItem}
              keyExtractor={this._keyExtractor}
            />
          ) : (
            <View
              style={{
                width,
                height: height * 0.1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text style={{ fontSize: size.regular, color: "#000" }}>
                No tienes cotizaciones realizadas.
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  backButton: {
    width,
    height: height * 0.1,
    justifyContent: "center"
  },
  backIcon: {
    tintColor: "#fff",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  },
  list: {
    height: height,
    width
  },
  loaderContainer: {
    height: height * 0.3,
    justifyContent: "center"
  }
});
