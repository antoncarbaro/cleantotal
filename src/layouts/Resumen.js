import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";

import constants from "../config/constants";
import img from "../config/images";
import url from "../config/url";

const { height, width, dotsIndex, size } = constants;
import { Navigation } from "react-native-navigation";

export default class Resumen extends Component {
  constructor(props) {
    super(props);
    this.returnToHome = this.returnToHome.bind(this);
  }

  returnToHome() {
    console.log("props", this.props);
    Navigation.push(this.props.componentId, {
      component: {
        name: "clean.Home"
      }
    });
  }

  render() {
    console.log(this.props);
    return (
      <View
        style={{
          flex: 1,
          height: height * 0.8,
          width,
          paddingTop: 15
        }}
      >
        <View
          style={{
            width: width * 0.9,
            height: height * 0.6,
            paddingLeft: 20,
            alignSelf: "center",
            borderRadius: 5,
            borderWidth: 1,
            borderColor: "#b7b7b7"
          }}
        >
          <View
            style={{
              width: width * 0.8,
              height: height * 0.1,
              justifyContent: "center"
            }}
          >
            <Text
              style={{
                textTransform: "uppercase",
                fontSize: size.regular,
                fontWeight: "bold"
              }}
            >
              {" "}
              RESUMEN DE LA COTIZACIÓN{" "}
            </Text>
          </View>
          <View
            style={{
              width: width * 0.8,
              height: height * 0.18,
              justifyContent: "space-evenly"
            }}
          >
            <Text>{this.props.roomNumber} habitación(es) </Text>
            <Text>{this.props.bathroomNumber} baños(s) </Text>
            <Text>{this.props.kitchenNumber} cocina(s) </Text>
            <Text>{this.props.salonNumber} sala(s)</Text>
          </View>
          <View
            style={{
              width: width * 0.8,
              height: height * 0.08,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between"
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: size.regular }}>
              Metros cuadrados
            </Text>
            <Text>{this.props.quarterMetters} mts2</Text>
          </View>
          <View
            style={{
              width: width * 0.8,
              height: height * 0.08,
              alignItems: "center",
              justifyContent: "space-between",
              flexDirection: "row"
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: size.regular }}>
              Rango de horas estimada
            </Text>
            <Text>
              {this.props.hours === 1 && "12 horas"}
              {this.props.hours === 2 && "24 horas"}
              {this.props.hours === 3 && "36 horas"}
            </Text>
          </View>
          <View
            style={{
              width: width * 0.75,
              height: height * 0.08,
              alignItems: "center",
              justifyContent: "space-between",
              flexDirection: "row"
            }}
          >
            <Text style={{ fontWeight: "bold" }}>Llevar productos</Text>
            <Text>{this.props.supplies ? "SI" : "NO"}</Text>
          </View>
          <View
            style={{
              width: width * 0.8,
              height: height * 0.08,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: size.normal }}>
              TOTAL
            </Text>
            <Text>S/. {this.props.precioFinal.toFixed(2)}</Text>
          </View>
        </View>
        <View
          style={{
            width,
            height: height * 0.2,
            alignItems: "center",
            justifyContent: "center",
            marginTop: 20
          }}
        >
          <TouchableOpacity
            onPress={this.returnToHome}
            style={{
              backgroundColor: "#469CDD",
              height: height * 0.06,
              width: width * 0.5,
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 5
            }}
          >
            <Text style={{ color: "#fff" }}>Regresar a Home</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
