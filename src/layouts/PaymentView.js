import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  WebView,
  AsyncStorage
} from "react-native";
import { Navigation } from "react-native-navigation";

import url from "../config/url";
import img from "../config/images";
import constants from "../config/constants";

const { height, width, size, appraiseArray } = constants;
const { paymentUrl } = url;

export default class PaymentView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "",
      user: {}
    };

    this._renderLoading = this._renderLoading.bind(this);
  }

  componentDidMount() {
    this.getUserStorage();
  }

  async getUserStorage() {
    let user = await JSON.parse(await AsyncStorage.getItem("user"));
    this.setState({ user }, () => this.setPaymentUrl());
  }

  async setPaymentUrl() {
    let payment = await fetch("https://api.culqi.com/v2/tokens", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer pk_test_VZX1aZbVwAU7QN5l`
      },
      body: JSON.stringify({
        card_number: "4111111111111111",
        cvv: "123",
        expiration_month: "09",
        expiration_year: "2020",
        email: "richard@piedpiper.com"
      })
    });

    console.log(payment.json());

    // const url = `${paymentUrl}${this.props.appraiseId}/${this.state.user.id}`;
    // this.setState({
    //   url
    // });
  }

  _renderLoading() {
    return (
      <View style={styles.loaderContainer}>
        <ActivityIndicator size="large" color="#469CDD" />
      </View>
    );
  }

  render() {
    return (
      <View style={{ height, width }}>
        <View
          style={{
            backgroundColor: "#469CDD",
            width,
            height: height * 0.08,
            justifyContent: "center"
          }}
        >
          <TouchableOpacity
            onPress={() => Navigation.pop(this.props.componentId)}
            style={styles.backButton}
          >
            <Image source={img.backIcon} style={styles.backIcon} />
          </TouchableOpacity>
        </View>
        {/* <WebView
          // scalesPageToFit
          javaScriptEnabled={true}
          startInLoadingState
          // mixedContentMode={"compatibility"}
          // injectedJavaScript={Constants.javascript.injection}
          // onMessage={event => console.log(event.nativeEvent.data)}
          // ignoreSslError
          renderLoading={this._renderLoading}
          onNavigationStateChange={e => {
            console.log(e);
          }}
          // onLoad={() => alert("termino")}
          source={{
            uri: this.state.url
          }}
          // style={{ marginTop: 0 }}
        /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  backButton: {
    width,
    height: height * 0.1,
    justifyContent: "center"
  },
  backIcon: {
    tintColor: "#fff",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  },
  list: {
    height: height,
    width
  },
  loaderContainer: {
    height: height * 0.3,
    justifyContent: "center"
  }
});

const Constants = {
  javascript: {
    injection: `
          Array.from(document.getElementsByTagName('input')).forEach((item) => {
              if(item.type == "search") {
                  item.value = "look at me, I'm injecting";
              }
          })
      `
  }
};
