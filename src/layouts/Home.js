import React from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  AsyncStorage
} from "react-native";
import { Navigation } from "react-native-navigation";

import { goToAuth } from "./Navigation";

import img from "../config/images";

import constants from "../config/constants";

const { height, width, size, appraiseArray } = constants;

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.navigationEventListener = Navigation.events().bindComponent(this);
    this.state = {
      user: {},
      user_social: null,
      appraiseArray: []
    };
    this.navigateToAppraise = this.navigateToAppraise.bind(this);
    this.navigateToList = this.navigateToList.bind(this);
    this.navigateToSetup = this.navigateToSetup.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidAppear() {
    this.getUserStorage();
  }

  async getUserStorage() {
    let user = await JSON.parse(await AsyncStorage.getItem("user"));
    let user_social = await JSON.parse(
      await AsyncStorage.getItem("user_social")
    );
    console.log("user", user);
    console.log("user_social", user_social);
    this.setState({ user, user_social });
  }

  navigateToAppraise() {
    Navigation.push(this.props.componentId, {
      component: {
        name: "clean.Appraise"
      }
    });
  }

  navigateToList() {
    Navigation.push(this.props.componentId, {
      component: {
        name: "clean.List"
      }
    });
  }

  renderSeparator() {
    return (
      <View
        style={{
          width: 10
        }}
      />
    );
  }

  async logout() {
    await AsyncStorage.removeItem("user");
    goToAuth();
  }

  navigateToSetup() {
    Navigation.push(this.props.componentId, {
      component: {
        name: "clean.Setup"
      }
    });
  }

  render() {
    const {
      user: { gender }
    } = this.state;
    const user_social = this.state.user_social || null;
    return (
      <View style={styles.imageBackgroundContainer}>
        <View
          style={{
            backgroundColor: "#469CDD",
            width,
            height: height * 0.08,
            alignItems: "center",
            justifyContent: "space-between",
            marginBottom: 15,
            flexDirection: "row"
          }}
        >
          <TouchableOpacity style={styles.backButton} />
          <View
            style={{
              width: width * 0.7,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                color: "#fff",
                fontWeight: "bold",
                fontSize: size.normal
              }}
            >
              Home
            </Text>
          </View>
          <View
            style={{
              width: width * 0.2,
              height: height * 0.1
            }}
          />
        </View>
        <View style={styles.userContainer}>
          <View style={styles.imageContainer}>
            <View
              style={{
                width: width * 0.365,
                height: height * 0.25
              }}
            />
            <View style={styles.profileBorder}>
              {!user_social ? (
                <Image
                  source={gender === "M" ? img.maleUser : img.femaleUser}
                  style={styles.profileImage}
                />
              ) : (
                <Image
                  source={{ uri: user_social.picture.data.url }}
                  style={styles.profileImage}
                />
              )}
            </View>
            <View style={styles.logoutContainer}>
              <TouchableOpacity activeOpacity={0.8} onPress={this.logout}>
                <Image source={img.powerIcon} style={styles.logoutIcon} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.usernameContainer}>
            <Text style={styles.usernameText}>
              {this.state.user.name} {this.state.user.last_name}
            </Text>
          </View>
        </View>
        <View style={styles.homeContainer}>
          <View
            style={{
              width: width * 0.8,
              height: height * 0.45,
              alignItems: "center",
              marginBottom: 15,
              justifyContent: "flex-end"
            }}
          >
            <TouchableOpacity
              onPress={this.navigateToSetup}
              activeOpacity={0.6}
              style={[styles.appraiseButton, { marginBottom: 15 }]}
            >
              <Text style={styles.appraiseText}>Configuración</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.navigateToList}
              activeOpacity={0.6}
              style={styles.appraiseButton}
            >
              <Text style={styles.appraiseText}>Ver cotizaciones</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={this.navigateToAppraise}
            activeOpacity={0.6}
            style={styles.appraiseButton}
          >
            <Text style={styles.appraiseText}>Cotizar ya!</Text>
          </TouchableOpacity>
        </View>
        {/* </View> */}
      </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.3)"
  },
  imageBackgroundContainer: {
    flex: 1
  },
  userContainer: {
    width,
    height: height * 0.33,
    alignItems: "center",
    flexDirection: "column"
  },
  imageContainer: {
    width,
    height: height * 0.2,
    flexDirection: "row",
    alignItems: "flex-end"
  },
  profileBorder: {
    borderRadius: 50,
    width: 103,
    height: 103,
    borderWidth: 5,
    borderColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  profileImage: {
    height: 97.5,
    width: 97.5,
    borderRadius: 50,
    resizeMode: "cover"
  },
  usernameContainer: {
    width,
    height: height * 0.05,
    justifyContent: "center",
    alignItems: "center"
  },
  usernameText: {
    fontSize: size.normal,
    color: "#000",
    fontWeight: "300"
  },
  logoutContainer: {
    width: width * 0.365,
    height: height * 0.25,
    justifyContent: "center",
    alignItems: "center"
  },
  logoutIcon: {
    resizeMode: "contain",
    width: width * 0.1,
    height: height * 0.05,
    tintColor: "#acd7ef"
  },
  homeContainer: {
    height: height * 0.5,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  appraiseButton: {
    width: width * 0.7,
    height: height * 0.08,
    backgroundColor: "#469CDD",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  appraiseText: {
    fontSize: size.normal,
    fontWeight: "300",
    textAlign: "center",
    color: "#fff"
  },
  backButton: {
    width: width * 0.15,
    height: height * 0.1,
    justifyContent: "center"
  },
  backIcon: {
    tintColor: "#fff",
    width: width * 0.12,
    height: height * 0.035,
    resizeMode: "contain"
  }
});
