import React from "react";
import {
  View,
  Image,
  ImageBackground,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  KeyboardAvoidingView
} from "react-native";

import constants from "../config/constants";
import img from "../config/images";
import url from "../config/url";

import { Navigation } from "react-native-navigation";
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from "react-native-fbsdk";

import { goHome } from "./Navigation";

const { height, width } = constants;
const { loginSocial } = url;

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loader: false
    };
    this.loginSocial = this.loginSocial.bind(this);
    this.toRegisterScreen = this.toRegisterScreen.bind(this);
    this.loginCallback = this.loginCallback.bind(this);
  }

  login() {
    this.setState({ loader: true });
    if (!this.state.email.length && !this.state.password.length) {
      this.setState({ loader: false });
      Alert.alert(
        "Datos invalidos",
        "Por favor completar los campos requeridos."
      );
      return;
    }

    this.asyncLogin();
  }

  async asyncLogin() {
    this.setState({ loader: true });
    let objAuth = {
      email: this.state.email.trim(),
      password: this.state.password.trim()
    };

    let responseLogin = await fetch(url.login, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(objAuth)
    });
    let res = await responseLogin.json();
    if (res.statusCode === 200) {
      this.setState({ loader: false });
      AsyncStorage.setItem("user", JSON.stringify(res.data));
      goHome();
    } else {
      this.setState({ loader: false });
      Alert.alert("Error!", res.message, [{ text: "OK" }]);
    }
  }

  async loginSocial() {
    let result;
    try {
      this.setState({ showLoadingModal: true });
      LoginManager.setLoginBehavior("NATIVE_ONLY");
      result = await LoginManager.logInWithReadPermissions([
        "public_profile",
        "email"
      ]);
    } catch (nativeError) {
      try {
        LoginManager.setLoginBehavior("WEB_ONLY");
        result = await LoginManager.logInWithReadPermissions([
          "public_profile",
          "email"
        ]);
      } catch (webError) {
        console.log("error", webError);
      }
    }

    if (result.isCancelled) {
      console.log("is cancelled");
    } else {
      this.graphRequest(
        "id, email, name, first_name, last_name, picture.type(large)",
        this.loginCallback.bind(this)
      );
    }
  }

  async graphRequest(fields, callback) {
    const accessData = await AccessToken.getCurrentAccessToken();
    const infoRequest = new GraphRequest(
      "/me",
      {
        accessToken: accessData.accessToken,
        parameters: {
          fields: {
            string: fields
          }
        }
      },
      callback
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  }

  async loginCallback(error, result) {
    if (error) {
      console.log(error);
    } else {
      console.log(result);

      let objLoginSocial = {
        type_login: 1,
        auth_token: result.id,
        email: result.email
      };

      let responseLoginSocial = await fetch(loginSocial, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(objLoginSocial)
      });
      let res = await responseLoginSocial.json();
      if (res.statusCode === 200) {
        AsyncStorage.setItem("user_social", JSON.stringify(result));
        AsyncStorage.setItem("user", JSON.stringify(res.data));
        goHome();
      }
    }
  }

  toRegisterScreen() {
    Navigation.push(this.props.componentId, {
      component: {
        name: "clean.Register"
      }
    });
  }

  render() {
    return (
      <ImageBackground source={img.backgroundLogin} style={{ flex: 1 }}>
        <KeyboardAvoidingView
          keyboardVerticalOffset={250}
          behavior="padding"
          enabled
          style={{
            width,
            height: height * 0.26,
            justifyContent: "flex-start",
            alignItems: "flex-start"
          }}
        >
          <Image
            source={img.logoCleaner}
            style={{
              width: width * 0.55,
              height: height * 0.3,
              resizeMode: "contain"
            }}
          />
        </KeyboardAvoidingView>
        <KeyboardAvoidingView
          keyboardVerticalOffset={50}
          behavior="padding"
          enabled
          style={{
            width,
            height: height * 0.35,
            justifyContent: "flex-end",
            alignItems: "center"
          }}
        >
          <View
            style={{
              height: height * 0.07,
              width: width * 0.9,
              flexDirection: "row",
              justifyContent: "center",
              backgroundColor: "#dcdada1c",
              borderColor: "#fff",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 5
            }}
          >
            <View
              style={{
                width: width * 0.12,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image
                source={img.userIcon}
                style={{
                  tintColor: "#fff",
                  height: height * 0.03,
                  resizeMode: "contain"
                }}
              />
            </View>
            <View style={{ width: width * 0.78 }}>
              <TextInput
                style={{ color: "#fff", height: height * 0.07 }}
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
                placeholder={"Correo"}
                placeholderTextColor={"#fff"}
              />
            </View>
          </View>
          <View
            style={{
              height: height * 0.07,
              width: width * 0.9,
              flexDirection: "row",
              justifyContent: "center",
              backgroundColor: "#dcdada1c",
              borderColor: "#fff",
              borderWidth: 1,
              borderRadius: 5,
              marginVertical: 5
            }}
          >
            <View
              style={{
                width: width * 0.12,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image
                source={img.lockIcon}
                style={{
                  tintColor: "#fff",
                  height: height * 0.03,
                  resizeMode: "contain"
                }}
              />
            </View>
            <View style={{ width: width * 0.78 }}>
              <TextInput
                style={{ color: "#fff", height: height * 0.07 }}
                secureTextEntry
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                placeholder={"Contraseña"}
                placeholderTextColor={"#fff"}
              />
            </View>
          </View>
        </KeyboardAvoidingView>
        <View
          style={{
            width,
            height: height * 0.1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            activeOpacity={0.7}
            style={{
              backgroundColor: "#fff",
              justifyContent: "center",
              alignItems: "center",
              width: width * 0.9,
              height: height * 0.07,
              borderRadius: 5
            }}
            onPress={this.login.bind(this)}
          >
            {this.state.loader ? (
              <ActivityIndicator size="large" color="#469CDD" />
            ) : (
              <Text style={{ color: "#469CDD", fontSize: 15 }}>Ingresar</Text>
            )}
          </TouchableOpacity>
        </View>
        <View
          style={{
            bottom: 0,
            width,
            height: height * 0.22,
            flexDirection: "row",
            alignItems: "flex-end",
            justifyContent: "space-evenly"
          }}
        >
          <TouchableOpacity
            onPress={this.loginSocial}
            style={{
              width: width * 0.5,
              height: height * 0.05,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "#fff" }}>Ingresa con facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={this.toRegisterScreen}
            style={{
              width: width * 0.5,
              height: height * 0.05,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{ color: "#fff" }}>Registrate aquí</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
