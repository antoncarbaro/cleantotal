import React, { Component } from "react";
import { View, ScrollView, TouchableOpacity, Text } from "react-native";
import { Navigation } from "react-native-navigation";

import config from "../config/constants";
import img from "../config/images";
import WelcomePage from "../components/WelcomePage";

const { width, height, welcomeDotsIndex, size } = config;

export default class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dotsIndex: [],
      index: 1
    };

    this.scrollTo = this.scrollTo.bind(this);
    this.toLoginScreen = this.toLoginScreen.bind(this);
  }

  componentDidMount() {
    this.setState({ dotsIndex: welcomeDotsIndex });
  }

  scrollTo(event) {
    let index = (event.nativeEvent.contentOffset.x / width + 1).toFixed(0);
    this.setState({ index });
  }

  toLoginScreen() {
    Navigation.push(this.props.componentId, {
      component: {
        name: "clean.Login"
      }
    });
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <ScrollView
          ref="scroll_welcome"
          onMomentumScrollEnd={this.scrollTo}
          horizontal={true}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
        >
          <WelcomePage
            index={this.state.index}
            toLoginScreen={this.toLoginScreen}
            welcomeText={"Bienvenido a CleanTotal"}
            imageLogo={img.logo1}
          />
          <WelcomePage
            index={this.state.index}
            toLoginScreen={this.toLoginScreen}
            welcomeText={"La aplicación de servicio del hogar por excelencia"}
            imageLogo={img.logo2}
          />
          <WelcomePage
            index={this.state.index}
            toLoginScreen={this.toLoginScreen}
            welcomeText={"Contamos con los mejores colaboradores"}
            imageLogo={img.logo3}
          />
        </ScrollView>
        <View
          style={{
            width,
            height: height * 0.1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          {+this.state.index === 3 && (
            <TouchableOpacity
              onPress={this.toLoginScreen}
              style={{
                width: width * 0.4,
                height: height * 0.06,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 5,
                backgroundColor: "#469CDD"
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: size.regular,
                  fontWeight: "400"
                }}
              >
                Empezar
              </Text>
            </TouchableOpacity>
          )}
        </View>
        <View
          style={{
            alignSelf: "center",
            width: width * 0.1,
            height: height * 0.12,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          {this.state.dotsIndex.map(value => (
            <View
              key={value}
              style={{
                backgroundColor:
                  +this.state.index === value ? "#469CDD" : "#c3c3c3",
                width: +this.state.index === value ? 10 : 8,
                height: +this.state.index === value ? 10 : 8,
                borderRadius: 5
              }}
            />
          ))}
        </View>
      </View>
    );
  }
}
