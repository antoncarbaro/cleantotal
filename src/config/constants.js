import { Dimensions, Platform } from "react-native";

const { width, height } = Dimensions.get("window");
const baseWidth = 375;
const baseHeight = 667;
const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);
const scaledSize = size => Math.ceil(size * scale);

const size = {
  mini: scaledSize(9.5),
  tiny: scaledSize(10),
  small: scaledSize(11),
  exact: scaledSize(12),
  fixed: scaledSize(12.5),
  little: scaledSize(13),
  regular: scaledSize(14.5),
  normal: scaledSize(17),
  max: scaledSize(20),
  great: scaledSize(28)
};

const dotsIndex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const welcomeDotsIndex = [1, 2, 3];

const publicKeyCulqi = "sk_test_v3ArceOQ45GmAjbP";

const appraiseArray = [
  { key: "1", text: "Cotizacion 1" },
  { key: "2", text: "Cotizacion 2" },
  { key: "3", text: "Cotizacion 3" },
  { key: "4", text: "Cotizacion 4" },
  { key: "5", text: "Cotizacion 5" },
  { key: "6", text: "Cotizacion 6" }
];

export default {
  size,
  width,
  scaledSize,
  height,
  topBarHeight: Platform.OS === "ios" ? height * 0.11 : height * 0.115,
  buttonSize: size * 0.14,
  dotsIndex,
  welcomeDotsIndex,
  appraiseArray,
  publicKeyCulqi
};
