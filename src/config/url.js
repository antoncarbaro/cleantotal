const generalUrl = "http://35.247.204.245/api/";
const culqiUrl = "https://api.culqi.com/v2/tokens";

const register = `${generalUrl}register`;
const login = `${generalUrl}login`;
const appraise = `${generalUrl}cotizador`;
const getAppraise = `${generalUrl}getPasarela`;
const paymentUrl = `${generalUrl}viewPasarela`;
const loginSocial = `${generalUrl}loginSocial`;
const updateProfile = `${generalUrl}update_profile`;

export default {
  register,
  appraise,
  login,
  culqiUrl,
  getAppraise,
  paymentUrl,
  loginSocial,
  updateProfile
};
