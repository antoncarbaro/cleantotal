const image = {
  backgroundLogin: require("../assets/img/background.jpg"),
  splashBackground: require("../assets/img/background-logo.jpg"),
  logoCleaner: require("../assets/img/logo.png"),
  userIcon: require("../assets/img/icon/avatar.png"),
  lockIcon: require("../assets/img/icon/lock.png"),
  rightIcon: require("../assets/img/icon/right.png"),
  maleUser: require("../assets/img/male_user.png"),
  femaleUser: require("../assets/img/female_user.png"),
  backIcon: require("../assets/img/icon/back.png"),
  powerIcon: require("../assets/img/icon/power.png"),
  logo1: require("../assets/img/logo_1.jpg"),
  logo2: require("../assets/img/logo_2.jpg"),
  logo3: require("../assets/img/logo_3.jpg"),
  downIcon: require("../assets/img/icon/down.png"),
  fridgeIcon: require("../assets/img/icon/fridge.png"),
  ironingIcon: require("../assets/img/icon/ironing.png"),
  ovenIcon: require("../assets/img/icon/oven.png"),
  wardroupeIcon: require("../assets/img/icon/wardroupe.png"),
  windowIcon: require("../assets/img/icon/window.png")
};

export default image;
