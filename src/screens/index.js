import { Navigation } from "react-native-navigation";
import Splash from "../layouts/Splash";
import Login from "../layouts/Login";
import Home from "../layouts/Home";
import Appraise from "../layouts/Appraise";
import Welcome from "../layouts/Welcome";
import Register from "../layouts/Register";
import Payment from "../layouts/Payment";
import Map from "../layouts/Map";
import List from "../layouts/List";
import PaymentView from "../layouts/PaymentView";
import Setup from "../layouts/Setup";
import Update from "../layouts/Update";

export function registerScreens() {
  Navigation.registerComponent("clean.Splash", () => Splash);
  Navigation.registerComponent("clean.Login", () => Login);
  Navigation.registerComponent("clean.Home", () => Home);
  Navigation.registerComponent("clean.Appraise", () => Appraise);
  Navigation.registerComponent("clean.Welcome", () => Welcome);
  Navigation.registerComponent("clean.Register", () => Register);
  Navigation.registerComponent("clean.Payment", () => Payment);
  Navigation.registerComponent("clean.Map", () => Map);
  Navigation.registerComponent("clean.List", () => List);
  Navigation.registerComponent("clean.PaymentView", () => PaymentView);
  Navigation.registerComponent("clean.Setup", () => Setup);
  Navigation.registerComponent("clean.Update", () => Update);
}
