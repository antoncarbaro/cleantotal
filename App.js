import { Navigation } from 'react-native-navigation';
import { registerScreens } from './src/screens';

registerScreens();

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setDefaultOptions({
        layout: {
            orientation: ['portrait'] 
        },
        topBar: {
            visible: false,
            drawBehind: true,
            animate: false,
        }
    });
    
    Navigation.setRoot({
        root: {
            stack: {
                children: [
                    {
                        component: {
                            id: "Splash",
                            name: 'clean.Splash'
                        }
                    }
                ]
            },
        }
    });
});
